#!/usr/bin/python
import MySQLdb
import time

class databaseMigrator():
    db_handler = {}
    try_count = 0
    
    def dbConnect(self):
        if self.try_count > 10:
            print "Couldn't connect to database, it doesn't seem to be running."
            return
        
        print "Attempting to connect to DB"
        
        try:
            self.try_count += 1
            self.db_handler = MySQLdb.connect(host="172.18.0.19", user="root", passwd="testpass", db="", connect_timeout=6) 
            self.runMigration()
        except MySQLdb.OperationalError:
            print "Couldn't connect to DB, retrying in 3 seconds."
            time.sleep(3)
            self.dbConnect()
            
    def runMigration(self):
        cursor = self.db_handler.cursor()
        cursor.execute("SET sql_notes = 0")
        cursor.execute("CREATE DATABASE IF NOT EXISTS wp_web1")
        cursor.execute('DELETE FROM mysql.user WHERE User=""')
        
        cursor.execute("CREATE USER IF NOT EXISTS 'web1'@'%' IDENTIFIED BY 'testpass'")
        cursor.execute("FLUSH PRIVILEGES")
        cursor.execute("GRANT CREATE,INSERT,SELECT,UPDATE,DELETE ON wp_web1.* TO 'web1'@'%'")
        
        self.db_handler.commit()
        self.db_handler.close()
    
databaseMigrator().dbConnect()