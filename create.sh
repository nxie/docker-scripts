volume_dir=/home/volumes/
container_dir=/home/containers/
pull_dir=/home/docker
primary_disk=/dev/vda1
cron_dir=/home/docker-scripts/crons/
database_dir=/home/database
key_api_url=https://api.wordpress.org/secret-key/1.1/salt/

rm -f /etc/sysctl.conf
touch /etc/sysctl.conf
#https://docs.docker.com/engine/reference/commandline/dockerd/
echo "net.core.somaxconn = 65532" >> /etc/sysctl.conf
echo "net.ipv4.tcp_max_tw_buckets = 1440000" >> /etc/sysctl.conf
echo "net.ipv4.tcp_fin_timeout = 15" >> /etc/sysctl.conf
echo "net.ipv4.tcp_window_scaling = 1" >> /etc/sysctl.conf
echo "net.ipv4.tcp_max_syn_backlog = 3240000" >> /etc/sysctl.conf
echo "soft nofile 4096" >> /etc/security/limits.conf
echo "hard nofile 4096" >> /etc/security/limits.conf
sysctl -p

if [ -n "$1" ]; then
    #Disable SSH password authentication

    if [ ! -f "/etc/ssh/sshd_config_bak" ]; then
        cp /etc/ssh/sshd_config /etc/ssh/sshd_config_bak
    fi
    
    cp /etc/ssh/sshd_config_bak /etc/ssh/sshd_config
    
    echo "ChallengeResponseAuthentication no" >> /etc/ssh/sshd_config
    echo "PasswordAuthentication no" >> /etc/ssh/sshd_config
    echo "UsePAM no" >> /etc/ssh/sshd_config
    
    echo "MaxAuthTries 1" >> /etc/ssh/sshd_config
    /etc/init.d/sshd restart
fi

systemctl stop postfix
yum remove -y postfix

yum install -y epel-release
yum install -y python-pip
yum install -y git
pip install --upgrade pip
yum install -y python-devel mysql-devel
yum install -y gcc gcc-c++
pip install MySQL-python

cd /home

if [ ! -d "$container_dir" ]; then
  mkdir $container_dir
fi

if [ ! -d "$volume_dir" ]; then
  mkdir $volume_dir
fi

if [ ! -d "$database_dir" ]; then
  mkdir $database_dir
fi

rm -R -f "${database_dir}/*"

if [ -d "$pull_dir-wordpress" ]; then
    rm -Rf "${pull_dir}-wordpress"
fi

git clone https://nxie@bitbucket.org/nxie/docker-wordpress.git

if [ ! -d "$pull_dir-scripts" ]; then
    git clone https://nxie@bitbucket.org/nxie/docker-scripts.git
fi

crontab -l > "${cron_dir}new_cron"
echo "20 4 * * * /bin/bash ${cron_dir}daily.sh" >> "${cron_dir}new_cron"
crontab "${cron_dir}new_cron"
rm "${cron_dir}new_cron"

docker network create --subnet=172.18.0.0/16 nxnet
docker run -d -v /home/docker-wordpress/export.sql:/home/export.sql -v $database_dir:/var/lib/mysql --name db -e MYSQL_ROOT_PASSWORD=testpass --net nxnet --ip 172.18.0.19 percona:5.7.15

python $pull_dir-scripts/setup_db.py

for i in 1 2
do
    mount_path="${container_dir}web$i"

    if [ -d $mount_path ]; then
        if mount | grep $mount_path > /dev/null; then
            umount -l $mount_path
        fi
        rm -R -f $mount_path
    fi
    
    mkdir $mount_path
    
    if [ -n "$1" ]; then
        
        vol_path="${volume_dir}web$i.ext3"
    
        if [ -f $vol_path ]; then
            rm -f $vol_path
        fi
    
        touch $vol_path
        dd if=$primary_disk of=$vol_path bs=200M count=24
        mkfs.ext4 -F $vol_path
        mount -o loop,rw,usrquota,grpquota $vol_path $mount_path
        rm -R -f "${mount_path}/lost+found"
    fi
    
    cp -a "${pull_dir}-wordpress/." $mount_path

    if [ -f "${mount_path}/db.sql" ]; then
        rm "${mount_path}/db.sql"
    fi
    
    #Randomize wordpress installation salts
    echo $(wget $key_api_url -q -O -) >> "${mount_path}/wp-config-keys.php"
    
    docker run -d --name="web$i" -v $mount_path:/var/www --net nxnet --ip 172.18.0.2$i -e SERVER_NAME="dev.nx.ie" -e DB_NAME="wp_web$i" -e DB_HOST=172.18.0.19 -e DB_USER="web$i" -e DB_PASS=testpass -c 512 -m 576m --oom-kill-disable --memory-reservation 384m nxie/nginx-php-fpm
done

docker exec db sh -c "exec mysql -uroot -p\"testpass\" wp_web1 < /home/export.sql"
docker run -d --name=proxy -p 80:80 --net nxnet --ip 172.18.0.20 -v /home/docker-scripts/conf/haproxy.cfg:/etc/haproxy/haproxy.cfg:ro -v /home/docker-scripts/conf/mappings.cfg:/etc/haproxy/mappings.cfg janeczku/alpine-haproxy:1.6

iptables -N SSHATTACK
iptables -A SSHATTACK -j LOG --log-prefix "Possible SSH attack! " --log-level 7
iptables -A SSHATTACK -j DROP

iptables -A INPUT -i eth0 -p tcp -m state --dport 22 --state NEW -m recent --set
iptables -A INPUT -i eth0 -p tcp -m state --dport 22 --state NEW -m recent --update --seconds 240 --hitcount 3 -j SSHATTACK